# Data generation

Data is of the format

```json
[
  {
    "t_end": 1.0,
    "pfd": 700
  },
]
```

`python main.py`

For more advanced uses check the supplied help

`python main.py --help`
