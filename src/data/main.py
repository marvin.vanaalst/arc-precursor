from __future__ import annotations

import json
from pathlib import Path
import typer


def create_data(
    n_pulses: int,
    pulse_length: float,
    pulse_intensity: float,
    actinic_length: float,
    actinic_intensity: float,
) -> dict[float, float]:
    t: float = 1.0
    data: dict[float, float] = {t: 0}
    for _ in range(n_pulses):
        t += pulse_length
        data[t] = pulse_intensity
        t += actinic_length
        data[t] = actinic_intensity
    return data


def main(
    out_file: Path = Path(__file__).parent / "data.json",
    n_pulses: int = 5,
    pulse_length: float = 0.8,
    pulse_intensity: float = 5000,
    actinic_length: float = 30,
    actinic_intensity: float = 50,
) -> None:
    raw_data = create_data(
        n_pulses=n_pulses,
        pulse_length=pulse_length,
        pulse_intensity=pulse_intensity,
        actinic_length=actinic_length,
        actinic_intensity=actinic_intensity,
    )

    json_data = [{"t_end": k, "pfd": v} for k, v in raw_data.items()]
    with open(Path(out_file), "w") as fp:
        json.dump(
            json_data,
            fp,
            indent=2,
        )


if __name__ == "__main__":
    typer.run(main)
