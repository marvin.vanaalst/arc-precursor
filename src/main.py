from __future__ import annotations

import matplotlib.pyplot as plt
import numpy as np
import pickle
import json
import typer
from adapters import AbstractAdapter, Matuszynska2016Adapter, Matuszynska2019Adapter, Saadat2021Adapter
from enum import Enum
from modelbase.ode import Simulator
from pathlib import Path

app = typer.Typer()


class ModelChoice(str, Enum):
    matuszynska2016 = "2016"
    matuszynska2019 = "2019"
    saadat2021 = "2021"


def get_adapter(adapter_name: ModelChoice) -> AbstractAdapter:
    match adapter_name:
        case ModelChoice.matuszynska2016:
            model_adapter: AbstractAdapter = Matuszynska2016Adapter()
        case ModelChoice.matuszynska2019:
            model_adapter = Matuszynska2019Adapter()
        case ModelChoice.saadat2021:
            model_adapter = Saadat2021Adapter()
    return model_adapter


@app.command()
def run_simulation(
    model: ModelChoice = ModelChoice.matuszynska2016,
    # in_file: Path = Path(__file__).parent / "data" / "data.p",
    # out_file: Path = Path(__file__).parent / "output" / "result.p",
) -> None:
    model_adapter = get_adapter(model)

    with open(Path(__file__).parent / "data" / "data.json", "rb") as fp:
        json_data: list[dict[str, float]] = json.load(fp)
        data = {i["t_end"]: i["pfd"] for i in json_data}

    s = Simulator(model_adapter.get_model())
    s.initialise(model_adapter.get_initial_conditions())
    for t_end, pfd in data.items():
        s.update_parameter(model_adapter.get_pfd_name(), pfd)
        s.simulate(t_end)

    assert (result := s.get_full_results_df()) is not None
    result = model_adapter.add_light_to_results(result, s)

    with open(Path(__file__).parent / "output" / f"result-{model.value}.p", "wb") as fp:
        pickle.dump(result, fp)


@app.command()
def plot_npq(
    model: ModelChoice = ModelChoice.matuszynska2016,
    # in_file: Path = Path(__file__).parent / "output" / "result.p",
    # out_file: Path = Path(__file__).parent / "output" / "result.png",
) -> None:
    model_adapter = get_adapter(model)

    with open(Path(__file__).parent / "output" / f"result-{model.value}.p", "rb") as fp:
        data = pickle.load(fp)

    lights = data[model_adapter.get_light_name()]
    fluo = data[model_adapter.get_fluorescence_name()]

    mask = (lights == lights.max()).values
    borders = np.logical_xor(mask[:-1], mask[1:])
    points_of_change = lights.index[np.where(borders)[0]]
    start_stop = list(zip(points_of_change[::2], points_of_change[1::2]))
    peaks = [fluo[start:stop].idxmax() for start, stop in start_stop]
    fm = fluo.loc[peaks]
    npq = (fm.iloc[0] - fm) / fm

    ax = npq.plot(title="NPQ", xlabel="time / s")
    for (t_start, t_end) in start_stop:
        ax.axvspan(
            t_start,
            t_end,
            facecolor=(0, 0, 0, 1 / 8),
            edgecolor=None,
        )
    plt.savefig(Path(__file__).parent / "output" / f"result-{model.value}.png")
    plt.close()


if __name__ == "__main__":
    app()
