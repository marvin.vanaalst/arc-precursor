from abc import ABC, abstractmethod
from modelbase.ode import Model, _Simulate
import pandas as pd


class AbstractAdapter(ABC):
    @abstractmethod
    def get_model(self) -> Model:
        ...

    @abstractmethod
    def get_initial_conditions(self) -> dict[str, float]:
        ...

    @abstractmethod
    def get_pfd_name(self) -> str:
        ...

    @abstractmethod
    def get_light_name(self) -> str:
        ...

    @abstractmethod
    def get_fluorescence_name(self) -> str:
        ...

    @abstractmethod
    def add_light_to_results(self, result: pd.DataFrame, s: _Simulate) -> pd.DataFrame:
        ...
