# Matuszyńska 2016

Implementation of model published [here](https://doi.org/10.1016/j.bbabio.2016.09.003).

## Setting up

- `pip install pre-commit`
- `pre-commit install`
- `pip install -r code/requirements.txt`
