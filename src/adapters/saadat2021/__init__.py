import numpy as np
import pandas as pd
from ..abstract import AbstractAdapter
from .src.model import get_model
from modelbase.ode import Model, _Simulate


class Saadat2021Adapter(AbstractAdapter):
    def get_model(self) -> Model:
        # why is the type any?
        return get_model()  # type: ignore

    def get_initial_conditions(self) -> dict[str, float]:
        return {
            "PQ": 11.027139850905353,
            "PC": 1.8895071932002812,
            "Fd": 3.8690237263896705,
            "ATP": 1.620195002854852,
            "NADPH": 0.4882103700673736,
            "H": 0.0022147075094596015,
            "LHC": 0.8023074419510501,
            "Psbs": 0.9607146039898598,
            "Vx": 0.950783616933656,
            "PGA": 0.9913970817549008,
            "BPGA": 0.0005355311557548053,
            "GAP": 0.0062630116252017295,
            "DHAP": 0.13778623933075737,
            "FBP": 0.006126990841013743,
            "F6P": 0.31166103888161867,
            "G6P": 0.7168203893211117,
            "G1P": 0.041575582577936025,
            "SBP": 0.01311315151803723,
            "S7P": 0.15782894767619207,
            "E4P": 0.00732079113061801,
            "X5P": 0.022396849486562384,
            "R5P": 0.03751472214765548,
            "RUBP": 0.13153657267999222,
            "RU5P": 0.015005888732707041,
            "MDA": 5.85270097771621e-06,
            "ASC": 9.999994138785292,
            "H2O2": 3.4273920330125316e-06,
            "DHA": 8.513863740903352e-09,
            "GSH": 9.999999991725186,
            "GSSG": 4.137406632226743e-09,
            "TR_ox": 0.9,
            "E_inactive": 4.7368421052631575,
        }

    def get_pfd_name(self) -> str:
        return "pfd"

    def get_light_name(self) -> str:
        return "L"

    def get_fluorescence_name(self) -> str:
        return "Fluo"

    def add_light_to_results(self, result: pd.DataFrame, s: _Simulate) -> pd.DataFrame:
        df = pd.DataFrame(
            np.concatenate(
                [
                    (t, np.full(shape=len(t), fill_value=p["pfd"]))
                    for t, p in zip(s.get_time(concatenated=False), s.simulation_parameters)  # type: ignore
                ],
                axis=1,
            ).T,
            columns=["time", "L"],
        ).set_index("time")
        result["L"] = df["L"]
        return result
