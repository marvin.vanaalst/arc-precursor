import pandas as pd
from ..abstract import AbstractAdapter
from .src.model import get_model
from modelbase.ode import Model, _Simulate


class Matuszynska2016Adapter(AbstractAdapter):
    def get_model(self) -> Model:
        # why is the type any?
        return get_model()  # type: ignore

    def get_initial_conditions(self) -> dict[str, float]:
        return {"P": 0, "H": 6.32975752e-05, "E": 0, "A": 25.0, "Pr": 1, "V": 1}

    def get_pfd_name(self) -> str:
        return "PFD"

    def get_light_name(self) -> str:
        return "L"

    def get_fluorescence_name(self) -> str:
        return "Fluo"

    def add_light_to_results(self, result: pd.DataFrame, s: _Simulate) -> pd.DataFrame:
        return result
