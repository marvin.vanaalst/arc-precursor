mamba activate pam

# Generate data
echo "$(date): generating data"
python src/data/main.py

# Run simulations
echo "$(date): running 2016 model"
python src/main.py run-simulation --model 2016
python src/main.py plot-npq --model 2016

echo "$(date): running 2019 model"
python src/main.py run-simulation --model 2019
python src/main.py plot-npq  --model 2019

echo "$(date): running 2021 model"
python src/main.py run-simulation --model 2021
python src/main.py plot-npq  --model 2021
