# PAM analyis ARC precursor

## Run

All `main.py` files are written using the excellent [typer](https://typer.tiangolo.com/) library, so you can always get help by writing `python main.py --help`

- Create and activate virtual environment (optional, but recommended)
  - Install mamba from [here](https://github.com/conda-forge/miniforge#mambaforge)
  - `mamba create -c conda-forge -n pam python=3.11 assimulo`
  - `mamba activate pam`
- Install dependencies with `pip install -r requirements.txt`
- Run all simulations with `./run-all.sh`

